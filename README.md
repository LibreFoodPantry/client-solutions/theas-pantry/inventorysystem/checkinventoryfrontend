# Example

The project set up is featured in the src/frontend directory to be able to run the program.

## Project setup

```bash
npm install
```

### Compiles and hot-reloads for development

```bash
npm run serve
```

### Compiles and minifies for production

```bash
npm run build
```

### Lints and fixes files

```bash
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# Based off of this tutorial:

https://github.com/mdobydullah/run-vue.js-app-with-docker
