# Developer Cheat Sheet

If any of these commands give you the message
"Permission denied", then run the following:

```bash
chmod +x bin/[insert file name]
```

## Backend-data

Allows you to load sample data into the backend servers.

```bash
bin/backend-data.sh
```

## Backend-down

Removes the Docker containers and networks.

The database will be emptied of all data.

```bash
bin/backend-down.sh
```

## Backend-restart

Takes down Docker containers and networks and restarts the Docker containers and networks.

**Use this command if you have not modified files in `src` and want to restart the local server.**

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
bin/backend-restart.sh
```

## Backend-up

Starts a locally running CheckInventoryFront server, along with InventoryBackend, a MongoDB database, a RabbitMQ message queue, and networks to connect them.

This will continue to run, and generate logging messages, in your terminal until it is terminated with Ctrl+C.

```bash
bin/backend-up.sh
```

## Frontend-dev-up

Builds the frontend in development mode which will hot reload the
servers and frontend to reflect ant changes made in the files.

```bash
bin/frontend-dev-up.sh
```

## Frontend-prod-build

Builds the frontend in production mode to see what the final product
would look like.

```bash
bin/frontend-prod-build.sh
```

## Frontend-prod-down

Breaks down the frontend that was running in production mode.

```bash
bin/frontend-prod-down.sh
```

## Frontend-prod-restart

Restarts the frontend servers while in production mode.

```bash
bin/frontend-prod-restart.sh
```

## Frontend-prod-up

Starts up the frontend servers in production mode.

```bash
bin/frontend-prod-up.sh
```

## Squash commits to prepare for merge into main

Before merging a merge request, use the following command to squash its
commits into a single commit, writing a good conventional-commit message.

```bash
bin/premerge-squash.sh
```
